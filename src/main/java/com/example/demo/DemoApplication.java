package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("mample")
	public String home() throws InterruptedException {
		RestTemplate restTemplate = new RestTemplateBuilder().setConnectTimeout(5000).build();
		RestTemplate restTemplate1 = new RestTemplate();
		ResponseEntity<String> response = restTemplate.getForEntity("http://10.245.151.241:8080/sample", String.class);
		return "mample" + response.getBody();
	}

	@GetMapping("ok")
	public String ok()
	{
		return "ok";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
